
#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <time.h>
#include <math.h>
int arret = 1e6;
int clientsServis;
long int cumule;
long int temps;
long int n;//nb clients dans la file au temps t
long int temps_file_vide;
double p0;
double p2;


int nb_arrivees(float p0, float p2){
	double r = (double)random()/RAND_MAX;
	if(r<p0)	
		return 0;
	if(r<p0+p2)
		return 2;
	return 1;
}

void arrivee_event(){
	n += nb_arrivees(p0, p2);//une arrivee a ajouter
	cumule += n;//pour ccl moyenne
}	

void service_event(){
	if(n>0) {//si il y a des gens dans la file
		n--;
		arret--;
	}
}
long int compteur = 0;
double proba_vide() {
	return (double)temps_file_vide/temps;
}


int condition_arret(long double new, long double old) {
	if(fabs((double)old-new)<1e-3 && temps > 1000){
		compteur++;
		if(compteur > 1e5){
			return 1;
		}
	}
	return 0;
}


void simulateur(FILE *f1){
	
	arret = 1e6;
	clientsServis = 0;
	cumule = 0;
	temps = 1;
	n = 0;//nb clients dans la file au temps t
	temps_file_vide = 0;
	long double oldNbMoyen = 0;
	long double Nmoyen = RAND_MAX;
	while(/*arret>0*/condition_arret(Nmoyen, oldNbMoyen) == 0){ //arret
		//printf("%d %d\n",n, arret);
		arrivee_event();
		oldNbMoyen = Nmoyen;
		Nmoyen = (long double)cumule/temps;
		fprintf(f1, "%5ld %Lf\n",temps, Nmoyen);
		service_event();
	if(n==0)
		temps_file_vide++;
		temps++;
	}
}

int main(int argc, char *argv[]) {
	srandom(getpid());
	p0 = 0.4;
	p2 = 0.35;
	FILE *f = fopen("data/simulation_cst.data","w");
	simulateur(f);
	fclose(f);
	printf("proba file vide :  %lf\n", proba_vide());
	return 0;
	
    
    
    
}
