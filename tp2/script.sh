#!/bin/bash
gcc poisson.c -lm
if [ "$#" -eq 0 ]
then
    #echo "Pas d'arguments".
    ./a.out
else 
    if [ "$#" -gt 0 ] 
    then
        echo $1
         ./a.out "$1"
    fi
rm *out
fi
R CMD BATCH DrawMM1.R
