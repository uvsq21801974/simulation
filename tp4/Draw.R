# pour exec R CMD BATCH "Draw.R"

modelname = "data_exponentielle.data"
data = read.table(modelname)
attach(data);

x=V1
f1=V2
f2=V3
plot(x,f1,type="l",xlab = "x", ylab="Fx(x)",col = "red", main="FONCTION DE REP DE LA LOI EXPO, lambda = 1")
lines(x,f2, type = "l", col = "blue")
legend("topleft", legend = c{"theorique", "experimental"}, col=c{"red", "blue"}, lty=1:1, cex=0)
