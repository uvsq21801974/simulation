
#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <time.h>
#include <math.h>
#define Lambda 1.0
#define TAILLEMAX 1E2

long double *valeurs;
long double *fct_rep;
long double *fct_rep2;

void init(){
	srandom(getpid());
	valeurs = (long double *)malloc(TAILLEMAX * sizeof(long double));
	fct_rep =(long double *)malloc(TAILLEMAX * sizeof(long double));
	fct_rep2 =(long double *)malloc(TAILLEMAX * sizeof(long double));
}

long double exponentielle(){
	long double r = (long double)random()/RAND_MAX;//[0..1]
	while(r==0 || r==1) {
		 r = (long double)random()/RAND_MAX;
	}
	long double res = -logl(r)/(Lambda*1.0);
	printf("expo %.15Lf, r %Lf\n", res, r);
	return res;//r unif sur 0..1
}

long double fct_repartition(long double x) {
	return 1 - exp(-((long double)Lambda)*x);
}


void fct_experimentale(){
	for(int i = 0 ;i<TAILLEMAX;i++){
		fct_rep2[i] = ((long double)(i+1)/TAILLEMAX);
	}
}
void generation_valeurs(){
	for(int i = 0; i<TAILLEMAX ; i++) {
		valeurs[i] = exponentielle();
	}
}
void tri_valeurs() {
	int i,j;
	long double temp1;
	for(i = 0 ; i < TAILLEMAX ; i++) {
		for(j=0; j<TAILLEMAX-i; j++){
			
			if(valeurs[j]>valeurs[j+1]){
				temp1 = valeurs[j+1];
				valeurs[j+1] = valeurs[j];
				valeurs[j] = temp1;
			}
		}	
	}
	for(i=0;i<TAILLEMAX;i++){
		fct_rep[i] = fct_repartition(valeurs[i]);//theorique
	}
}
void fct_theorique() {
	
	init();
	generation_valeurs();
	tri_valeurs();
}
void ecrire_valeurs(){
	
	FILE *f = fopen("data_exponentielle.data", "w");
	for(int i = 0 ; i<TAILLEMAX ; i++) {
		fprintf(f,"%.15Lf	%.15Lf	%.15Lf \n", valeurs[i],fct_rep[i], fct_rep2[i]);
	}
	fclose(f);
}

void Moyenne_exp(){
	long double somme = 0;
	for(int i = 0 ; i< TAILLEMAX;i++) {
		somme += valeurs[i]*(1.0/TAILLEMAX);
	}
	printf("Moyenne = %Lf \n", somme);
}

void affichertableaux(long double *tab) {
	for(int i = 0; i< TAILLEMAX; i++) {
		printf("%.3Lf\n",tab[i]);
	}
}
int main() {
	
	fct_theorique();
	printf("test0\n");
	fct_experimentale();
	
	affichertableaux(fct_rep2);
	printf("test1\n");
	ecrire_valeurs();
	
	printf("test2\n");
	return 0;
}
