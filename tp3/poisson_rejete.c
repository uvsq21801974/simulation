
#include <stdio.h>


#include <stdlib.h> 
#include <unistd.h>
#include <time.h>
#include <math.h>


//#define lambda 7
int lambda = 7;
int capacite = 10;
int s1 = 0;//1 si serveur 1libre 0 sinon
int s2 = 0;
#define mu 10//service
#define EPSILON 1e-5
#define MAXEVENT 1000000
#define MAXTEMPS 5000
#define B 100
int compteur = 0; //cond darret //condition de stabilité lamba inférieur a mu
int clientsServis;
double cumule = 0;
double temps = 0;
long int nrejet = 0;
long int narrivees = 0;
long int n = 0;//nb clients dans la file au temps t

typedef struct Event {
	int type;//0arrivee 1 service1 2 service2
	double date;
	int etat;//0 por non trait& et 1 pour traité
}event;

typedef struct Echeancier {
	event Tab[MAXEVENT];
	int taille;
} echeancier;

echeancier Ech;

double Exponentielle(int lbda){
	double r = (double)random()/RAND_MAX;//[0..1]
	while(r==0 || r==1) {
		 r = (double)random()/RAND_MAX;
	}
	double res = -log(r)/(lbda*1.0);
	printf("expo %f, r %f\n", res, r);
	return res;//r unif sur 0..1
}

void ajouter_ech(event e){
	if(Ech.taille < MAXEVENT) {
		Ech.Tab[Ech.taille] = e;
		Ech.taille++;
		printf("taille = %d\n", Ech.taille);
	}else{
		printf("echeancier vide\n");
	}	
}

void init_ech(){
	event e;
	e.type = 0;
	e.date = 0; 
	e.etat = 0;
	Ech.taille = 0;
    ajouter_ech(e);
}

void arrivee_event(event e){
	printf("\tarrivee client AC :");
	narrivees++;
	if( n  == B ){
		nrejet++;//a enleve si capacit& infinie
		printf("\t refus \n");
		return;
	}
	
	printf("\t accepté \n");
	n++;//on incremente le nb de clients dans la file
	event e1;
	e1.type = 0;
	e1.date = e.date + Exponentielle(lambda);//a cette nouvelle date on aura une autre arrivee
	e1.etat = 0;
	ajouter_ech(e1);
	if(n==1 && s1 == 0) {//si 1 seul evenement, on le traite FS tout de suite
		event e2;
		s1 = 1;
		e2.type = 1;
		e2.date = e.date + Exponentielle(mu);
		e2.etat = 0;
		ajouter_ech(e2);
	}
	temps = e.date;
}	

void service1_event(event e){//client tjrs considere dans la file
	printf("execution du service\n");
	if(s2 == 0){
		event e1;
		e1.type = 2;
		e1.date = e.date + Exponentielle(mu);
		e1.etat = 0;
		s2 = 1;
		ajouter_ech(e1);
		s1 = 0;
	}
	//////////////////////////////////
	else{
		///*
		event e1;
		e1.type = 1;
		e1.date = e.date + Exponentielle(mu);
		e1.etat = 0;
		ajouter_ech(e1);
		//*/temps attente tempsattente =+ e.t - temps;
	}

	temps = e.date;
}
	

void service2_event(event e){
	printf("execution du service\n");
	if(n>0){
		n--;
		s2 = 0;
		if(s1 == 1){
			event e1;
			e1.type = 2;
			e1.date = e.date + Exponentielle(mu);
			e1.etat = 0;
			s2 = 1;
			ajouter_ech(e1);
		}
		if(n>0 && s1 == 0){//si quelqu'un dans la file et personne dans s1
			event e1;
			e1.type = 1;
			e1.date = e.date + Exponentielle(mu);
			e1.etat = 0;
			ajouter_ech(e1);
			s1 = 1;
		}
		temps = e.date;
	}
}

void afficher_event(event e){
	if(e.type==0)
		printf(" (AC, %lf, %d),",e.date, e.etat);
	else if(e.type==1)
		printf(" (FS1, %lf, %d),",e.date, e.etat);
	else
		
		printf(" (FS2, %lf, %d),",e.date, e.etat);
}
void afficher_echeancier(){
	event e;
	printf("-->temps %f et N = %ld taille : %d [",temps, n, Ech.taille);
	for(int i = 0;i<Ech.taille;i++){
		e = Ech.Tab[i];
		afficher_event(e);
	}
	printf("]\n\n");
}
event extraire() {//etraire evennement lle plus ancien non traite
	event min;
	int i, imin;
	for( i = 0; i< Ech.taille; i++) {
		if(Ech.Tab[i].etat ==0){
			min = Ech.Tab[i];
			imin = i;
			break;
		}
	}
	
	for( /*tttt*/i = imin/*tttt*/; i< Ech.taille; i++) {
		if(min.date>Ech.Tab[i].date && Ech.Tab[i].etat == 0 ){
			min = Ech.Tab[i];
			imin = i;
		}
	}
	Ech.Tab[imin].etat = 1;//on le traite
	return min;
	
}

int condition_arret(long double new, long double old) {
	if(fabs((double)old-new)<EPSILON && temps > 1000){
		compteur++;
		if(compteur > 1e3){
			return 1;
		}
	}
	return 0;
}


void simul(FILE *f){
	long double oldMoy;
	long double NMoy;
	init_ech();
	event e;
	while(condition_arret(oldMoy, NMoy) == 0) {
		
		e =extraire();
		//afficher_event(e);
		cumule += (e.date-temps)*n;//duree de l'evt * n
		oldMoy = NMoy;
		NMoy = cumule/temps;
		//printf("cumule : %ld old : %Lf, nouv : %Lf\n", cumule, oldMoy, NMoy);
		//afficher_echeancier();
		if(temps == 0){
			printf("temps = 0 et N = 0 et Nmoye = 0\n");
			fprintf(f, "0    0\n");
		}else{
			printf("temps = %f et N = %ld et Nmoye = %Lf\n", temps, n, NMoy);
			fprintf(f, "%f    %Lf\n", temps, NMoy);
		}
		if(e.type==0){
			arrivee_event(e);
		}else if (e.type==1){
			service1_event(e);
		}else{
			service2_event(e);
		}
	}
}

int main(int argc, char *argv[]) {
	if(argc > 1){
		lambda = atoi(argv[1]);
		printf("lambda : %s\n", argv[1]);
	}
	srandom(getpid());
	FILE *f = fopen("simulation_MM1.data", "w");
	simul(f);
	fclose(f);
	return 0;
	
    
    
    
}
